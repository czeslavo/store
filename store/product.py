class Product(object):
    """Product with its name and price."""
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def __str__(self):
        return "%15s -- %d PLN" % (self.name, self.price)


class FoodProduct(Product):
    """Product with weight."""
    def __init__(self, name, price, weight):
        super().__init__(name, price)
        self.weight = weight

    def __str__(self):
        return  super().__str__() + " -- %d g" % self.weight