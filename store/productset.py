from store.product import Product, FoodProduct


class ProductSet(object):
    """Set of products."""

    def __init__(self, name):
        """
        :param name(str): name of the set
        """

        self.name = name
        self.products = set()

    def add_product(self, product):
        """Adds a product to the set.
        :param product: product to add
        """

        self.products.add(product)

    def get_product(self, name):
        """Gets product from the set by its name.
        :param name: name of the product to get
        """

        for product in self.products:
            if product.name == name:
                return product
        return None

    def remove_product(self, name_of_product, first=False):
        """Removes product from the set by name.
        :param name_of_product: name of the product to remove
        :param first: flag which tells if only the first occurrence
        should be removed or all of them
        """

        to_remove = set()
        for product in self.products:
            if product.name == name_of_product:
                to_remove.add(product)
                if first:
                    break
        self.products = self.products - to_remove

    def get_subset(self, predicate):
        """Get subset of the set which follows given predicate.
        :param predicate: predicate for product
        :return: subset
        """

        return filter(predicate, self.products)

    def get_cheapest(self):
        """
        :return: cheapest product in the set
        """

        if len(self.products) > 0:
            key = lambda product: product.price
            return min(self.products, key=key)
        return None

    def get_most_expensive(self):
        """
        :return: the most expensive product in the set
        """

        if len(self.products) > 0:
            key = lambda product: product.price
            return max(self.products, key=key)
        return None


class FoodStore(ProductSet):
    """Predefined food store with products.
    It can handle selling products
    """

    def __init__(self, name):
        super().__init__(name)
        self.__cash = 5000
        self.products = {
            FoodProduct('carrot', 2, 500),
            FoodProduct('milk', 5, 1000),
            FoodProduct('coca-cola', 10, 2000),
            FoodProduct('potato', 1, 50),
            FoodProduct('snickers', 10, 200)
        }


    def __str__(self):
        return "-- Welcome to " + self.name + " --\n" +\
              "\n".join(str(product) for product in self.products)

    def sell(self, name, cash):
        """Sells product to somebody who pays `cash`.
        :param name: name of the product to sell
        :param cash: amount of cash input
        :return: (change, product) if enough cash, otherwise None
        """

        product = self.get_product(name)
        if self.open and product and product.price <= cash:
            self.__cash += product.price
            return cash - product.price, product
        else:
            return None

    def get_3_cheapest(self):
        """
        :return: 3 cheapest products
        """
        return sorted(self.products, key=lambda x: x.price)[:3]

    def get_heaviest(self):
        """
        :return: the heaviest product
        """
        if len(self.products) > 0:
            return max(self.products, key=lambda x: x.weight)
        else:
            return None

    def get_best_ratio(self):
        """
        :return: item with the best weight/price ratio
        """
        return max(self.products, key=lambda x: x.weight / x.price)

    def close(self):
        """Closes the store for customers"""
        self.open = False

    def open(self):
        """Opens the store for customers"""
        self.open = True



if __name__ == "__main__":
    store = FoodStore('Grzegorzowy sklep')
    print(store)
    print(store.get_cheapest())
    print(store.get_heaviest())
    print(store.get_best_ratio())